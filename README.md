# Firebase Practice
My Android project to practice firebase connect to the app 

----

## Main goals
This part of app was created for CIC project( Clothes in Closet) . This is not a final version , but it�s a part of my personal
work . We have created it 2 month ago to participate in Hackaton event with my team A42 . My task was to build data logic for this app.
I solved the main issue - sync data from SQLite with Firebase (Online database) . While user adding new information to the app and he dont have connect to the internet then data will mark as 'not synced', so after
a couple of time when user connect  , information will be adding to Firebase with mark as 'synced' ( boolean type '0' as not synced and '1' as synced).

---

## Links
[This project on Github](https://github.com/lystive/CIC)